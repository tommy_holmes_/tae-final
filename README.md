**Please make your own branch, cheers**

**Pending Changes**

*  Sign in with Apple (trying to make it work with iOS 12) - see link: [Sign in with Apple iOS 12](https://developer.apple.com/documentation/sign_in_with_apple/sign_in_with_apple_js/incorporating_sign_in_with_apple_into_other_platforms)
*  Fix results page's title when no results are returned to say "No Results for start Chord to end chord"
*  Add the 2 gesture recognisers, was going to add a double tap to add to favourites in the song details controller 
*  More Unit testing needs adding to up the coverage
*  When pressing add to favourites on the song detials VC, needs visual confirmation that it's been added, and if the user pressed a second time it deletes from Core Data and gives user visual confirmation it has been deleted.
*  The auto layout messes up (shifts right) the image view on the favourties tab at runtime for some reason, needs fixing
*  Make sure the share feature works with social networks


**Nice to haves**

*  When users signs up it offers categories the user can chooes the 3+ favourites and an algoritm then reccommends music based on this in the 1st tab (rather than the 'profile' tab) - and this changes depending on who is logged in on Firebase, so this may need to use Firestore (pod should already be imported)
*  Favourites save for different users that login with firebase
*  Add a loading graphic when signing up/logging in and when loading results and song details VC
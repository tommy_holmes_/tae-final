//
//  DeeJaayTests.swift
//  DeeJaayTests
//
//  Created by Tom Holmes on 04/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import XCTest
import Alamofire
@testable import DeeJaay

//MARK: - LoginVCTests
class LoginVCTests: XCTestCase {
    
    var loginVC: LoginViewController!
    
    override func setUp() {
        super.setUp()
        
        loginVC = LoginViewController()
    }
    
    override func tearDown() {
        loginVC = nil
        
        super.tearDown()
    }
    
    
}


//MARK: - ChordProgressionsTests
class ChordProgressionsTests: XCTestCase {
    
    var chordModel: ChordProgressions!
    
    override func setUp() {
        super.setUp()
        chordModel = ChordProgressions()
    }
    
    override func tearDown() {
        chordModel = nil
        super.tearDown()
    }
    
    func testExpectedNumberOfChords() {
        let chords = chordModel.chords
        
        XCTAssertEqual(chords?.count, 14)
    }
    
    func testNumberOfRows() {
        let number = chordModel.numberOfRows()
        
        XCTAssertNotNil(number)
    }
    
    func testTitleOfRow1() {
        let title = chordModel.titleOfRow(at: 0)
        
        XCTAssertTrue(title == "Major: I")
    }
    
    func testTitleOfRow2() {
        let title = chordModel.titleOfRow(at: 1)
        
        XCTAssertTrue(title == "Major: ii")
    }
    
    func testTitleOfRow3() {
        let title = chordModel.titleOfRow(at: 2)
        
        XCTAssertTrue(title == "Major: iii")
    }
    
    func testTitleOfRow4() {
        let title = chordModel.titleOfRow(at: 3)
        
        XCTAssertTrue(title == "Major: IV")
    }
    
    func testTitleOfRow5() {
        let title = chordModel.titleOfRow(at: 4)
        
        XCTAssertTrue(title == "Major: V")
    }
    
    func testTitleOfRow6() {
        let title = chordModel.titleOfRow(at: 5)
        
        XCTAssertTrue(title == "Major: VI")
    }
    
    func testTitleOfRow7() {
        let title = chordModel.titleOfRow(at: 6)
        
        XCTAssertTrue(title == "Major: vii")
    }
    
    func testTitleOfRow8() {
        let title = chordModel.titleOfRow(at: 7)
        
        XCTAssertTrue(title == "Minor: i")
    }
    
    func testTitleOfRow9() {
        let title = chordModel.titleOfRow(at: 8)
        
        XCTAssertTrue(title == "Minor: iiº")
    }
    
    func testTitleOfRow10() {
        let title = chordModel.titleOfRow(at: 9)
        
        XCTAssertTrue(title == "Minor: bIII")
    }
    
    func testTitleOfRow11() {
        let title = chordModel.titleOfRow(at: 10)
        
        XCTAssertTrue(title == "Minor: iv")
    }
    
    func testTitleOfRow12() {
        let title = chordModel.titleOfRow(at: 11)
        
        XCTAssertTrue(title == "Minor: v")
    }
    
    func testTitleOfRow13() {
        let title = chordModel.titleOfRow(at: 12)
        
        XCTAssertTrue(title == "Minor: bVI")
    }
    
    func testTitleOfRow14() {
        let title = chordModel.titleOfRow(at: 13)
        
        XCTAssertTrue(title == "Minor: bVII")
    }
    
    func testChordID1() {
        let id = chordModel.chordId(for: "Major: I")
        
        XCTAssertEqual(id, "1")
    }
    
    func testChordID2() {
        let id = chordModel.chordId(for: "Major: ii")
        
        XCTAssertEqual(id, "2")
    }
    
    func testChordID3() {
        let id = chordModel.chordId(for: "Major: iii")
        
        XCTAssertEqual(id, "3")
    }
    
    func testChordID4() {
        let id = chordModel.chordId(for: "Major: IV")
        
        XCTAssertEqual(id, "4")
    }
    
    func testChordID5() {
        let id = chordModel.chordId(for: "Major: V")
        
        XCTAssertEqual(id, "5")
    }
    
    func testChordID6() {
        let id = chordModel.chordId(for: "Major: VI")
        
        XCTAssertEqual(id, "6")
    }
    
    func testChordID7() {
        let id = chordModel.chordId(for: "Major: vii")
        
        XCTAssertEqual(id, "7")
    }
    
    func testChordID8() {
        let id = chordModel.chordId(for: "Minor: i")
        
        XCTAssertEqual(id, "B1")
    }
    
    func testChordID9() {
        let id = chordModel.chordId(for: "Minor: iiº")
        
        XCTAssertEqual(id, "B2")
    }
    
    func testChordID10() {
        let id = chordModel.chordId(for: "Minor: bIII")
        
        XCTAssertEqual(id, "B3")
    }
    
    func testChordID11() {
        let id = chordModel.chordId(for: "Minor: iv")
        
        XCTAssertEqual(id, "B4")
    }
    
    func testChordID12() {
        let id = chordModel.chordId(for: "Minor: v")
        
        XCTAssertEqual(id, "B5")
    }
    
    func testChordID13() {
        let id = chordModel.chordId(for: "Minor: bVI")
        
        XCTAssertEqual(id, "B6")
    }
    
    func testChordID14() {
        let id = chordModel.chordId(for: "Minor: bVII")
        
        XCTAssertEqual(id, "B7")
    }
    
    func testTitleForRow1() {
        let title = chordModel.titleOfRow(at: 0)
        
        XCTAssertEqual(title, "Major: I")
    }
    
    func testTitleForRow2() {
        let title = chordModel.titleOfRow(at: 1)
        
        XCTAssertEqual(title, "Major: ii")
    }
    
    func testTitleForRow3() {
        let title = chordModel.titleOfRow(at: 2)
        
        XCTAssertEqual(title, "Major: iii")
    }
    
    func testTitleForRow4() {
        let title = chordModel.titleOfRow(at: 3)
        
        XCTAssertEqual(title, "Major: IV")
    }
    
    func testTitleForRow5() {
        let title = chordModel.titleOfRow(at: 4)
        
        XCTAssertEqual(title, "Major: V")
    }
    
    func testTitleForRow6() {
        let title = chordModel.titleOfRow(at: 5)
        
        XCTAssertEqual(title, "Major: VI")
    }
    
    func testTitleForRow7() {
        let title = chordModel.titleOfRow(at: 6)
        
        XCTAssertEqual(title, "Major: vii")
    }
    
    func testTitleForRow8() {
        let title = chordModel.titleOfRow(at: 7)
        
        XCTAssertEqual(title, "Minor: i")
    }
    
    func testTitleForRow9() {
        let title = chordModel.titleOfRow(at: 8)
        
        XCTAssertEqual(title, "Minor: iiº")
    }
    
    func testTitleForRow10() {
        let title = chordModel.titleOfRow(at: 9)
        
        XCTAssertEqual(title, "Minor: bIII")
    }
    
    func testTitleForRow11() {
        let title = chordModel.titleOfRow(at: 10)
        
        XCTAssertEqual(title, "Minor: iv")
    }
    
    func testTitleForRow12() {
        let title = chordModel.titleOfRow(at: 11)
        
        XCTAssertEqual(title, "Minor: v")
    }
    
    func testTitleForRow13() {
        let title = chordModel.titleOfRow(at: 12)
        
        XCTAssertEqual(title, "Minor: bVI")
    }
    
    func testTitleForRow14() {
        let title = chordModel.titleOfRow(at: 13)
        
        XCTAssertEqual(title, "Minor: bVII")
    }
    
    
}

//MARK: - ChordsPickerViewControllerTests
class ChordsPickerViewControllerTests: XCTestCase {
    
    var objectUnderTest: ChordsPickerViewController!
    
    override func setUp() {
        super.setUp()
        objectUnderTest = ChordsPickerViewController()
    }
    
    override func tearDown() {
        objectUnderTest = nil
        super.tearDown()
    }
    
    
}


//MARK: - HooktheoryManagerTests
class HooktheoryManagerTests: XCTestCase {
    
    
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testHooktheoryConnectionReturns200() {
        let url = URL(string: "https://api.hooktheory.com/v1/trends/songs?cp=4,1")!
        let headers: HTTPHeaders = [.authorization("Bearer d97d64846c19670d7a8acb0955f945f1")]
        
        let promise = expectation(description: "Status Code 200")
        
        var statusCode: Int?
        var responseError: Error?
        
        AF.request(url, headers: headers).responseJSON { response in
            
            statusCode = response.response?.statusCode
            responseError = response.error
            
            promise.fulfill()
            
        }
        
        
        
        waitForExpectations(timeout: 15.0, handler: nil)
        
        XCTAssertNil(responseError, "A network error")
        XCTAssertEqual(200, statusCode, "The code was 200")
    }
    
}

class SongDetialsModelTests: XCTestCase {
    
    var sut: SongDetailsModel!
    
    override func setUp() {
        super.setUp()
        
        sut = SongDetailsModel(CoreDataStack.shared)
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    
    
}


//MARK: - DeezerManagerTests
class DeezerManagerTests: XCTestCase {
    
    
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testDeezerConnectionReturns200() {
        let url = URL(string: "https://api.deezer.com/search/track?q=%22i%20need%20a%20dollar")!
        
        let promise = expectation(description: "Status Code 200")
        
        var statusCode: Int?
        var responseError: Error?
        
        AF.request(url).responseJSON { response in
            
            statusCode = response.response?.statusCode
            responseError = response.error
            
            promise.fulfill()
            
        }
        
        
        waitForExpectations(timeout: 15.0, handler: nil)
        
        XCTAssertNil(responseError, "A network error")
        XCTAssertEqual(200, statusCode, "The code was 200")
    }
    
}


//MARK: - SongDetailsViewControllerTests
class SongDetailsViewControllerTests: XCTestCase {
    
    var sut: SongDetailsViewController!
    
    override func setUp() {
        super.setUp()
        
        sut = SongDetailsViewController()
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    
}

//
//  SpotifyTrackIDManager.swift
//  DeeJaay
//
//  Created by Tom Holmes on 07/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation
import Alamofire

struct SongResult: Codable {
    let data: [DataObject]?
}

struct DataObject: Codable {
    let id: Int
    let title: String
    let duration: Int32
    let explicitLyrics: Bool
    let preview: URL
    let artist: Artist
    let album: Album
    let link: URL
    let favourite: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id, title, duration, preview, artist, album, link
        case explicitLyrics = "explicit_lyrics"
    }
}

struct Artist: Codable {
    let name: String
}

struct Album: Codable {
    let title: String
    let coverMedium: URL
    
    enum CodingKeys: String, CodingKey {
        case title
        case coverMedium = "cover_medium"
    }
}



class DeezerManager {
    
    var root: SongResult?
    
    func getResults(songName: String, artistName: String, completion: @escaping () -> Void) {
        
        var comps = URLComponents()
        
        let track = "\(songName) \(artistName)"
        let encodedTrackName = track.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        comps.scheme = "https"
        comps.host = "api.deezer.com"
        comps.path = "/search/track"
        comps.queryItems = [URLQueryItem(name: "q", value: encodedTrackName)]
        
        guard let url = comps.url else { return print("Unable to create URL \(comps)") }
        print(url)
        
        AF.request(url).responseJSON { response in
            if let data = response.data {
                self.parseJSON(data)
                completion()
            }
        }
    }
    
    
    func parseJSON(_ data: Data) {
        let jsonDecoder = JSONDecoder()
        
        do {
            let rootObject = try jsonDecoder.decode(SongResult.self, from: data)
            root = rootObject
            
        } catch {
            print(error)
        }
        
        
    }
}

//
//  HooktheoryManager.swift
//  DeeJaay
//
//  Created by Tom Holmes on 05/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation
import Alamofire

struct Results: Codable {
    let artist: String
    let song: String
    let section: String
}


class HooktheoryManager {
    
    var root: [Results]?
    
    func getResults(startChord: String, endChord: String, pageNo: Int, completion: @escaping () -> Void) {
        
        let token = "d97d64846c19670d7a8acb0955f945f1"
        var comps = URLComponents()
        
        comps.scheme = "https"
        comps.host = "api.hooktheory.com"
        comps.path = "/v1/trends/songs"
        comps.queryItems = [URLQueryItem(name: "cp", value: "\(startChord),\(endChord)"),
                            URLQueryItem(name: "page", value: "\(pageNo)")
        ]
        
        guard let url = comps.url else { return print("Unable to create URL \(comps)") }
        print(url)
        
        let headers: HTTPHeaders = [.authorization("Bearer \(token)")]
        
        AF.request(url, headers: headers).responseJSON { response in
            if let data = response.data {
                self.parseJSON(data)
                completion()
            }
        }
        
    }
    
    
    func parseJSON(_ data: Data) {
        let jsonDecoder = JSONDecoder()
        
        do {
            let rootObject = try jsonDecoder.decode([Results].self, from: data)
            root = rootObject
            
        } catch {
            print(error)
        }
        
    }
    
    
}


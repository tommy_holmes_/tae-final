//
//  ChordProgressions.swift
//  DeeJaay
//
//  Created by Tom Holmes on 05/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation

struct ChordProgressions {
    
    var hooktheoryManager: HooktheoryManager!
    var chords: [String]!
    var fetchedResults: [Results]!
    
    init() {
        hooktheoryManager = HooktheoryManager()
        chords = ["Major: I", "Major: ii", "Major: iii", "Major: IV", "Major: V", "Major: VI", "Major: vii",
                  "Minor: i", "Minor: iiº", "Minor: bIII", "Minor: iv", "Minor: v", "Minor: bVI", "Minor: bVII"
        ]
        
        // An array to store the results so that when more get added they are appended to the existing results
        fetchedResults = []
    }
    
}

//MARK: - UIPicker Helpers
extension ChordProgressions {
    
    func titleOfRow(at row: Int) -> String {
        chords[row]
    }
    
    func numberOfRows() -> Int {
        chords.count
    }
    
    func chordId(for chord: String) -> String {
        var id = ""
        
        switch chord {
        case "Major: I":
            id = "1"
        case "Major: ii":
            id = "2"
        case "Major: iii":
            id = "3"
        case "Major: IV":
            id = "4"
        case "Major: V":
            id = "5"
        case "Major: VI":
            id = "6"
        case "Major: vii":
            id = "7"
        case "Minor: i":
            id = "B1"
        case "Minor: iiº":
            id = "B2"
        case "Minor: bIII":
            id = "B3"
        case "Minor: iv":
            id = "B4"
        case "Minor: v":
            id = "B5"
        case "Minor: bVI":
            id = "B6"
        case "Minor: bVII":
            id = "B7"
        default:
            break
        }
        
        return id
    }
    
    func performNetworking(chord1: String, chord2: String, pageNo: Int, completion: @escaping () -> Void) {
        hooktheoryManager.getResults(startChord: chord1, endChord: chord2, pageNo: pageNo) {
            completion()
        }
        
    }
    
}


//MARK: - TableView Helpers
extension ChordProgressions {
    
    var numberOfSections: Int {
        return 1
    }
    
    var results: [Results] {
        guard let res = fetchedResults else { return [] }
        
        return res
    }
    
    var count: Int {
        results.count
    }
    
    func numberOfRows(in section:Int) -> Int {
        guard section >= 0 && section < numberOfSections else { return 0 }
        return results.count
    }
    
    func items(at indexPath: IndexPath) -> Results? {
        guard indexPath.row >= 0 && indexPath.row < numberOfRows(in: indexPath.section) else { return nil }
        
        return results[indexPath.row]
    }
    
}

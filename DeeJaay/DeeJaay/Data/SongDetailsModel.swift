//
//  SongDetailsModel.swift
//  DeeJaay
//
//  Created by Tom Holmes on 08/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation
import CoreData

class SongDetailsModel {
    
    var deezerManger: DeezerManager!
    var songResult: DataObject!
    let coreDataStack: CoreDataStack
    
    init(_ coreDataStack: CoreDataStack) {
        deezerManger = DeezerManager()
        self.coreDataStack = coreDataStack
    }
}


extension SongDetailsModel {
    
    func getSongDetails(songTitle: String, artistName: String, completion: @escaping () -> Void) {
        deezerManger.getResults(songName: songTitle, artistName: artistName) {
            if (self.deezerManger.root?.data!.isEmpty)! {
                // Deezer does not have the requested song :(
                completion()
                
            } else if let newResult = self.deezerManger.root?.data?[0] {
                // Deezer does have the requested song :)
                self.songResult = newResult
                completion()
            }
            
        }
        
    }
    
    func newFavourite(title: String?, artist: String?, album: String?, duration: Int32, isExplicit: Bool, cover: Data) {
        let mainContext = coreDataStack.mainContext
        let newFavourite = Song(context: mainContext)
        // Adding new favourtie to core data entity
        newFavourite.title = title
        newFavourite.album = album
        newFavourite.artist = artist
        newFavourite.duration = duration
        newFavourite.isExplicit = isExplicit
        newFavourite.cover = cover
        newFavourite.favourite.toggle()
        
        coreDataStack.saveContext()
    }
}

//
//  FavouritesTableViewController.swift
//  DeeJaay
//
//  Created by Tom Holmes on 13/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit
import CoreData

class FavouritesTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    // Injecting stack into VC to delete rows
    let model = SongDetailsModel(CoreDataStack.shared)
    
    // Vars to be selected
    var selectedSongTitle: String!
    var selectedSongArtist: String!
    
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController <Song> = {
        
        let fetchRequest: NSFetchRequest = Song.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: CoreDataStack.shared.mainContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()

    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.reloadData()
        tableView.tableFooterView = UIView()
        
        do {
            try fetchedResultsController.performFetch()
        } catch let fetchedError {
            print("Fetch error: \(fetchedError.localizedDescription)")
        }
        
        
    }

    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = fetchedResultsController.sections else { return 0 }
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let rows = fetchedResultsController.sections?[section].numberOfObjects else { return 0 }
        return rows
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FavouritesTableViewCell
        let song = fetchedResultsController.object(at: indexPath)
        
        guard let imageData = song.cover else { return cell }
        
        cell.titleLabel.text = song.title
        cell.albumLabel.text = song.album
        cell.artistLabel.text = song.artist
        cell.durationLabel.text = "\((song.duration % 3600) / 60)m \((song.duration % 3600) % 60)s"
        cell.explicitLabel.isHidden = !song.isExplicit
        cell.imageView?.image = UIImage(data: imageData)
        
        return cell
    }
    
    //MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSongTitle = fetchedResultsController.object(at: indexPath).title
        selectedSongArtist = fetchedResultsController.object(at: indexPath).artist
        
        performSegue(withIdentifier: "goToSong", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Remove") { (action, indexpath) in
            
            let objToDelete = self.fetchedResultsController.object(at: indexPath)
            self.fetchedResultsController.managedObjectContext.delete(objToDelete)
            self.model.coreDataStack.saveContext()
        }
        
        return [deleteAction]
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SongDetailsViewController {
            
            destinationVC.injectedSongTitle = selectedSongTitle
            destinationVC.injectedArtistName = selectedSongArtist
        }
    }
    

}


//MARK: - NSFetchedResultsControllerDelegate
extension FavouritesTableViewController {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
            case .insert:
                tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
            case .delete:
                tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
            case .move:
                break
            case .update:
                break
            @unknown default:
                fatalError("@unknown default")
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
            case .insert:
                if let newIndexPath = newIndexPath {
                    tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            case .delete:
                if let indexPath = indexPath {
                    tableView.deleteRows(at: [indexPath], with: .fade)
            }
            case .update:
                if let indexPath = indexPath {
                    tableView.reloadRows(at: [indexPath], with: .fade)
            }
            case .move:
                if let indexPath = indexPath, let newIndexPath = newIndexPath {
                    tableView.moveRow(at: indexPath, to: newIndexPath)
            }
            @unknown default:
                fatalError("@unknown default")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}

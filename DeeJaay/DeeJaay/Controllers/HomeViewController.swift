//
//  ViewController.swift
//  DeeJaay
//
//  Created by Tom Holmes on 04/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bioField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let profilePicture = getSavedImage(named: "profilePicture.png")
        imageView.image = profilePicture
        
        let userBio = getSavedBio(named: "userBio.txt")
        bioField.text = userBio
        
        // For demonstartion purposes
        nameLabel.text = "Tom Holmes"
    }
    
    
    //MARK: - IBActions
    @IBAction func changePicturePressed(_ sender: UIButton) {
        let picker = UIImagePickerController()
        picker.sourceType = .savedPhotosAlbum
        picker.allowsEditing = true
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    
    @IBAction func saveBioPressed(_ sender: UIButton) {
        if let newBio = bioField.text {
            saveBio(text: newBio)
            print("New bio saved")
        }
    }
    
}


//MARK: - UIImagePickerControllerDelegate
extension HomeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imageView.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        if let profilePic = imageView.image {
            saveImage(image: profilePic)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


//MARK: - Save and read profile picture and bio funcs
extension HomeViewController {
    
    func saveImage(image: UIImage) {
        // Putting image jpeg or png data into data constant
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            print("Data fail")
            return
        }
        // Retrieving user's document directory
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            print("Directory fail")
            return
        }
        
        // Attempting to write image data to user's doc directory
        do {
            try data.write(to: directory.appendingPathComponent("profilePicture.png")!)
            print("Image saved")
            return
        } catch {
            print(error.localizedDescription)
            return
        }
    }
    
    
    func getSavedImage(named: String) -> UIImage? {
        // Attempting to retrieve image from user's doc directory
        if let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            
            return UIImage(contentsOfFile: URL(fileURLWithPath: directory.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
    
    
    func saveBio(text: String) {
        // Putting bio utf8 data into data constant
        guard let bioData = bioField.text.data(using: .utf8) else {
            print("No valid text")
            return
        }
        // Retrieving user's document directory
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            print("Directory fail")
            return
        }
        
        // Attempting to write utf8 data to user's doc directory
        do {
            try bioData.write(to: directory.appendingPathComponent("userBio.txt")!)
            print("Bio saved")
            return
        } catch {
            print(error.localizedDescription)
            return
        }
    }
    
    
    func getSavedBio(named: String) -> String? {
        // Attempting to retrieve string from user's doc directory
        if let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            
            return try? String(contentsOf: URL(fileURLWithPath: directory.absoluteString).appendingPathComponent(named))
        }
        return nil
    }
    
}

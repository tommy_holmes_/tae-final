//
//  ChordsPickerViewController.swift
//  DeeJaay
//
//  Created by Tom Holmes on 06/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit

class ChordsPickerViewController: UIViewController {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var searchButton: UIButton!
    var model: ChordProgressions!
    var chord1: String!
    var chord2: String!
    var chordID1 = "1"
    var chordID2 = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchButton.layer.cornerRadius = 5

        model = ChordProgressions()
        
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    
    //MARK: - IBActions
    @IBAction func searchPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "goToResults", sender: self)
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SongResultsTableViewController {
            
            destinationVC.hooktheoryResults = model
            destinationVC.chord1 = chord1
            destinationVC.chord2 = chord2
            destinationVC.chordID1 = chordID1
            destinationVC.chordID2 = chordID2
        }
    }
    
    
}


//MARK: - UIPickerViewDataSource
extension ChordsPickerViewController: UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        model.numberOfRows()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int { 2 }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        model.titleOfRow(at: row)
    }
    
    
}


//MARK: - UIPickerViewDelegate
extension ChordsPickerViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedValueFirstRow = pickerView.selectedRow(inComponent: 0)
        let selectedValueSecondRow = pickerView.selectedRow(inComponent: 1)
        
        chord1 = model.chords[selectedValueFirstRow]
        chord2 = model.chords[selectedValueSecondRow]
        
        chordID1 = model.chordId(for: chord1)
        chordID2 = model.chordId(for: chord2)
    }
    
    
}

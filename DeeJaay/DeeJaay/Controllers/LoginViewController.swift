//
//  LoginViewController.swift
//  DeeJaay
//
//  Created by Tom Holmes on 15/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var appleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UI stuff
        loginButton.layer.cornerRadius = 5.0
        errorLabel.isHidden = true
        
        
    }
    
    
    //MARK: - IBActions
    @IBAction func loginPressed(_ sender: UIButton) {
        
        // Cleaned email and password
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        // Signing in the user
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            
            if error != nil {
                // Could not sign in
                self.errorLabel.text = error?.localizedDescription
                self.errorLabel.isHidden = false
            } else {
                self.performSegue(withIdentifier: "goToMain", sender: self)
            }
        }
        
    }
    
    
    @IBAction func signUpPressed(_ sender: UIButton) {
        
        // Create the user
        let alert = UIAlertController(title: "Register", message: "New User", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Sign Up", style: .default) { (action) in
            let emailField = alert.textFields![0]
            let passwordField = alert.textFields![1]
            
            Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
               
                if error != nil {
                    
                    if let errorCode = AuthErrorCode(rawValue: error!._code) {
                        switch errorCode {
                        case .weakPassword:
                            self.errorLabel.text = "Password too weak, needs 6+ charaters"
                            self.errorLabel.isHidden = false
                        default:
                            self.errorLabel.text = "There has been an error"
                            self.errorLabel.isHidden = false
                        }
                    }
                }
                
                if user != nil {
                    // TODO: Want to send a verification email here
                }
                
                // Transition to the home screen
                Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) { (result, error) in
                    
                    if error != nil {
                        // Could not sign in
                        self.errorLabel.text = error?.localizedDescription
                        self.errorLabel.isHidden = false
                    } else {
                        self.performSegue(withIdentifier: "goToMain", sender: self)
                    }
                    
                }
                
                
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addTextField { (textEmail) in
            textEmail.placeholder = "Enter your Email"
        }
        
        alert.addTextField { textPassword in
            textPassword.isSecureTextEntry = true
            textPassword.placeholder = "Enter your password"
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func signInWithApplePressed(_ sender: UIButton) {
        // TODO
    }
    
    
}



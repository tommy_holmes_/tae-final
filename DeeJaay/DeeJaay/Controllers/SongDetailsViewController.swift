//
//  SongDetailsViewController.swift
//  DeeJaay
//
//  Created by Tom Holmes on 08/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Social

class SongDetailsViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var songLabel: UILabel!
    @IBOutlet weak var explicitLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var audioView: UIView!
    @IBOutlet weak var addButton: UIButton!
    
    var model: SongDetailsModel!
    var injectedSongTitle: String!
    var injectedArtistName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        songLabel.text = "Loading..."
        artistLabel.text = ""
        albumLabel.text = ""
        durationLabel.text = ""
        explicitLabel.isHidden = true
        
        model = SongDetailsModel(CoreDataStack.shared)
        
        model.getSongDetails(songTitle: injectedSongTitle, artistName: injectedArtistName) {
            self.populate()
        }
        
    }
    
}


//MARK: - Populate func
extension SongDetailsViewController {
    
    func populate() {
        
        if (model.deezerManger.root?.data!.isEmpty)! {
            // No Deezer song result
            self.songLabel.text = "Song not found"
            self.artistLabel.text = "Please  try another"
            self.albumLabel.isHidden = true
            self.durationLabel.isHidden = true
            self.addButton.isHidden = true
            
        } else if let song: DataObject = model.songResult {
            // First song from Deezer search
            if song.explicitLyrics { self.explicitLabel.isHidden = false }
            
            let duration = song.duration
            let formattedDuration = "\((duration % 3600) / 60) minutes \((duration % 3600) % 60) seconds"
            
            let imageURL = song.album.coverMedium
            
            self.imageView.imageFromServerURL(imageURL, placeHolder: UIImage(named: "SongPlaceHolder"))
            self.songLabel.text = song.title
            self.albumLabel.text = song.album.title
            self.artistLabel.text = song.artist.name
            self.durationLabel.text = formattedDuration
            
            let player = AVPlayer(url: song.preview)
            let playerVC = AVPlayerViewController()
            playerVC.player = player
            self.present(playerVC, animated: true) {
                playerVC.player!.play()
            }
            
        }
        
    }
    
}


//MARK: - IBActions
extension SongDetailsViewController {
    
    @IBAction func addPressed(_ sender: UIButton) {
        if let song: DataObject = model.songResult {
            
            guard let imageData = imageView.image?.pngData() else { return }
            
            model.newFavourite(title: song.title, artist: song.artist.name, album: song.album.title, duration: song.duration, isExplicit: song.explicitLyrics, cover: imageData)
            
            
            //TODO: Add alertController to indicated added/removed from favs
        }
        
    }
    
    
    @IBAction func sharePressed(_ sender: UIButton) {
        guard let image = imageView.image else { return }
        
        let activityController = UIActivityViewController(activityItems: [model.songResult.title, model.songResult.artist, image, model.songResult.link], applicationActivities: nil)
        
        present(activityController, animated: true, completion: nil)
        //TODO: Add functionallity to ac
    }
    
}

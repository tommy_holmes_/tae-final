//
//  SongResultsTableViewController.swift
//  DeeJaay
//
//  Created by Tom Holmes on 05/04/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit


class SongResultsTableViewController: UITableViewController {
    
    // Injected results
    var hooktheoryResults: ChordProgressions?
    var chord1: String!
    var chord2:String!
    var chordID1: String!
    var chordID2: String!
    
    // Vars to be selected
    var selectedSongTitle: String!
    var selectedSongArtist: String!
    
    // Vars to be used in this VC
    private var fetchInProgress = false
    private var pageNo = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Performs search using Hooktheory's API with chords IDs from previous VC
        hooktheoryResults?.performNetworking(chord1: chordID1, chord2: chordID2, pageNo: pageNo, completion: {
            self.tableRefresh()
        })
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = hooktheoryResults?.numberOfRows(in: section) else { return 0 }
        
        return sections
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ResultsTableViewCell
        
        guard let item = hooktheoryResults?.items(at: indexPath) else { return cell }
        
        cell.songLabel.text = item.song
        cell.artistLabel.text = item.artist
        cell.sectionLabel.text = item.section
        
        return cell
    }
    
    
    
    //MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let indexPath = tableView.indexPathForSelectedRow else { return }
        // Gets title and artist to plug into segue
        selectedSongTitle = hooktheoryResults?.fetchedResults[indexPath.row].song
        selectedSongArtist = hooktheoryResults?.fetchedResults[indexPath.row].artist
        
        performSegue(withIdentifier: "goToSongDetails", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let count = hooktheoryResults?.hooktheoryManager.root?.count else { return }
        let lastItem = count - 1
        // Loads more results and adds cells to the list when user scrolls to the bottom
        if indexPath.row == lastItem {
            if !fetchInProgress {
                beginFetchingMore()
            }
            
        }
        
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SongDetailsViewController {
            // Sending song title and artist over to the player VC for Deezer to search
            destinationVC.injectedSongTitle = selectedSongTitle
            destinationVC.injectedArtistName = selectedSongArtist
        }
    }
    
    
}


//MARK: - API funcs
extension SongResultsTableViewController {
    
    func beginFetchingMore() {
        pageNo += 1
        print("Begin fetching batch")
        fetchInProgress = true
        
        hooktheoryResults?.performNetworking(chord1: chordID1, chord2: chordID2, pageNo: pageNo, completion: {
            self.tableRefresh()
            //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.fetchInProgress = false
            //}
            
        })
        
    }
    
    
    func tableRefresh() {
        
        if self.hooktheoryResults?.fetchedResults == nil {
            self.title = "No results for \(self.chord1 ?? "") → \(self.chord2 ?? "")"
            
        } else {
            DispatchQueue.main.async {
                guard let newResults = self.hooktheoryResults?.hooktheoryManager.root else { return }
                self.hooktheoryResults?.fetchedResults.append(contentsOf: newResults)
                self.tableView.reloadData()
            }
            
            self.title = "Results for \(self.chord1 ?? "") → \(self.chord2 ?? "")"
        }
        
    }
    
}
